<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {

	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => []], function ($api) {
		
        $api->resource('profiles', '\App\Http\Controllers\v1\ProfileController');
        $api->resource('tenants', '\App\Http\Controllers\v1\TenantController');
	});

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {
		// $api->post('timelog/capture', '\App\Http\Controllers\API\V1\TimelogController@capture')->name('timelog.capture');
		// $api->resource('timelog', '\App\Http\Controllers\API\V1\TimelogController');
	});
	
}