<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends \Sunnydevbox\TWCore\Http\Controllers\APIBaseController
{
    public function __construct(
        \App\Repositories\Profile\ProfileRepositoryEloquent $repository,
        \App\Transformers\ProfileTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }
}
