<?php

namespace App\Http\Controllers\v1;

use Dingo\Api\Http\Request;
use App\Http\Controllers\Controller;
use Hyn\Tenancy\Models\Website;

class TenantController extends \Sunnydevbox\TWCore\Http\Controllers\APIBaseController
{
    public function store(Request $request)
    {
        $tenant = $this->repository->makeModel();
        
        $result = $this->repository->create($tenant);

        dd($result->uuid);

    }


    public function __construct(
        \App\Repositories\Tenant\TenantRepositoryEloquent $repository,
        \App\Transformers\TenantTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }
}
