<?php

namespace App\Repositories\Tenant;

use Prettus\Repository\Contracts\RepositoryInterface;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;

use Hyn\Tenancy\Contracts\Website;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Tenant;

/**
 * Interface TenantRepository.
 *
 * @package namespace App\Repositories;
 */
interface TenantRepository 
    //extends WebsiteRepository
{
        /**
     * @param string $uuid
     * @return Website|null
     */
    public function findByUuid(string $uuid);

    /**
     * @param string|int $id
     * @return Website|null
     */
    public function findById($id);
    /**
     * @param Website $website
     * @return Website
     */
    public function create(Tenant &$website): Tenant;
    /**
     * @param Website $website
     * @return Website
     */
    public function update(Tenant &$website): Tenant;
    /**
     * @param Website $website
     * @param bool $hard
     * @return Website
     */
    public function delete(Tenant &$website, $hard = false): Tenant;

    /**
     * @warn Only use for querying.
     * @return Builder
     */
    public function query();
}
