<?php

namespace App\Repositories\Tenant;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Tenant\TenantRepository;
use App\Validators\TenantValidator;
use App\Models\Tenant;
use \Illuminate\Database\Eloquent\Builder;
use Hyn\Tenancy\Models\Website;


/**
 * Class TenantRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TenantRepositoryEloquent 
    // extends \Sunnydevbox\TWCore\Repositories\TWBaseRepository
    extends \Hyn\Tenancy\Repositories\WebsiteRepository
//     implements TenantRepository
{





    public function makeModel()
    {
        $model = $this->model();
        return new $model;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Website::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return TenantValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
