<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Tenant;

/**
 * Class TenantTransformer.
 *
 * @package namespace App\Transformers;
 */
class TenantTransformer extends TransformerAbstract
{
    /**
     * Transform the Tenant entity.
     *
     * @param \App\Entities\Tenant $model
     *
     * @return array
     */
    public function transform(Tenant $model)
    {
        return $model->toArray();
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
