###########################################################
###################### General Setup ######################
###########################################################

### Paths #################################################

APP_CODE_PATH_HOST=./src

# Point to where the `APP_CODE_PATH_HOST` should be in the container. You may add flags to the path `:cached`, `:delegated`. When using Docker Sync add `:nocopy`
APP_CODE_PATH_CONTAINER=/var/www/html:cached

# Choose storage path on your machine. For all storage systems
DATA_PATH_HOST=./_storage/db

# Define the prefix of container names. This is useful if you have multiple projects that use laradock to have seperate containers per project.
COMPOSE_PROJECT_NAME=azzister_ehr

### NGINX #################################################

NGINX_HOST_HTTP_PORT=80
NGINX_HOST_HTTPS_PORT=443
NGINX_HOST_LOG_PATH=./logs/nginx/
NGINX_SITES_PATH=./nginx/sites/
NGINX_PHP_UPSTREAM_CONTAINER=php-fpm
NGINX_PHP_UPSTREAM_PORT=9000
NGINX_SSL_PATH=./nginx/ssl/


### PHP_FPM ###############################################

PHP_FPM_INSTALL_ZIP_ARCHIVE=true
PHP_FPM_INSTALL_BCMATH=true
PHP_FPM_INSTALL_MYSQLI=true
PHP_FPM_INSTALL_INTL=true
PHP_FPM_INSTALL_IMAGEMAGICK=true
PHP_FPM_INSTALL_OPCACHE=true
PHP_FPM_INSTALL_IMAGE_OPTIMIZERS=true
PHP_FPM_INSTALL_PHPREDIS=true
PHP_FPM_INSTALL_MEMCACHED=false
PHP_FPM_INSTALL_XDEBUG=false
PHP_FPM_INSTALL_PHPDBG=false
PHP_FPM_INSTALL_IMAP=false
PHP_FPM_INSTALL_MONGO=false
PHP_FPM_INSTALL_AMQP=false
PHP_FPM_INSTALL_MSSQL=false
PHP_FPM_INSTALL_SSH2=false
PHP_FPM_INSTALL_SOAP=false
PHP_FPM_INSTALL_GMP=false
PHP_FPM_INSTALL_EXIF=false
PHP_FPM_INSTALL_AEROSPIKE=false
PHP_FPM_INSTALL_PGSQL=false
PHP_FPM_INSTALL_GHOSTSCRIPT=false
PHP_FPM_INSTALL_LDAP=false
PHP_FPM_INSTALL_PHALCON=false
PHP_FPM_INSTALL_SWOOLE=false
PHP_FPM_INSTALL_PG_CLIENT=false
PHP_FPM_INSTALL_PCNTL=false
PHP_FPM_INSTALL_CALENDAR=false
PHP_FPM_INSTALL_FAKETIME=false
PHP_FPM_INSTALL_IONCUBE=false
PHP_FPM_FAKETIME=-0
PHP_FPM_INSTALL_APCU=false
PHP_FPM_INSTALL_YAML=false


### MYSQL #################################################

MYSQL_VERSION=5.7
MYSQL_DATABASE=
MYSQL_USER=root
MYSQL_PASSWORD=
MYSQL_PORT=3308
MYSQL_ROOT_PASSWORD=root
MYSQL_ENTRYPOINT_INITDB=./mysql/docker-entrypoint-initdb.d